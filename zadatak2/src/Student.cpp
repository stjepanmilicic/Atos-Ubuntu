#include <iostream>
#include <cstring>
using namespace std;
#include "/home/stjepan/Desktop/zadatak2/include/Student/Student.h"

class Student {
    private:
        string studentName;
    public:
        string getName(){
            return studentName;
        }
        Student(){}
        ~Student(){}
        Student(string name){
            studentName = name;
        }
        friend ostream& operator<<(ostream& os, Student& student){
            os << "Student's name: " << student.getName() << "\n";
            return os;
        }
};
