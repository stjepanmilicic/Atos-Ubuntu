#include <iostream>
#include <cstring>
using namespace std;

#include "/home/stjepan/Desktop/zadatak5/include/Professor/Professor.h"

class Professor {
    private:
        string professorName;
    public:
        string getName(){
            return professorName;
        }
        Professor(){}
        ~Professor(){}
        Professor(string name){
            professorName = name;
        }
        friend ostream& operator<<(ostream& os, Professor& professor){
            os << "Professor's name: " << professor.getName() << "\n";
            return os;
        }
};

