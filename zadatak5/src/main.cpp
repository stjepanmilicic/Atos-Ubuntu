#include <iostream>
#include <cstring>
using namespace std;

#include "Professor.cpp"
#include "Student.cpp"

int main(){
    string name;
    cout << "Enter student's name: ";
    cin >> name;
    Student student(name);
    cout << "Enter professor's name: ";
    cin >> name;
    Professor professor(name);
    cout << "\n";
    cout << student;
    cout << professor;
    return 0;
}
