#include <iostream>
#include <cstring>
using namespace std;

#include "/home/stjepan/Desktop/zadatak6/include/Professor/Professor.h"
#include "/home/stjepan/Desktop/zadatak6/include/Student/Student.h"
#include "/home/stjepan/Desktop/zadatak6/lib/libProfessor.so"
#include "/home/stjepan/Desktop/zadatak6/lib/libStudent.so"


int main(){
    string name;
    cout << "Enter student's name: ";
    cin >> name;
    Student student(name);
    cout << "Enter professor's name: ";
    cin >> name;
    Professor professor(name);
    cout << "\n";
    cout << student;
    cout << professor;
    return 0;
}
