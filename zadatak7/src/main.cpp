#include <iostream>
#include <cstring>
using namespace std;

#include "/home/stjepan/Desktop/zadatak7/Professor/lib/libProfessor.so"
#include "/home/stjepan/Desktop/zadatak7/Student/lib/libStudent.so"
#include "/home/stjepan/Desktop/zadatak7/Professor/include/Professor/Professor.h"
#include "/home/stjepan/Desktop/zadatak7/Student/include/Student/Student.h"

int main(){
    string name;
    cout << "Enter student's name: ";
    cin >> name;
    Student student(name);
    cout << "Enter professor's name: ";
    cin >> name;
    Professor professor(name);
    cout << "\n";
    cout << student;
    cout << professor;
    return 0;
}
