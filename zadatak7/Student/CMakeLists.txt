cmake_minimum_required(VERSION 3.16.3)
project(zadatak7_Student)

set(CMAKE_CXX_STANDARD 11)
add_executable(zadatak7_Student src/Student.cpp)
